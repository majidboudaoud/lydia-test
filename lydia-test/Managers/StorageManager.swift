//
//  StorageManager.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import RealmSwift

struct StorageManager {
    
    let configuration: Realm.Configuration

    internal init(config: Realm.Configuration) {
      configuration = config
    }

    private var realm: Realm {
      return try! Realm(configuration: configuration)
    }

    private static let usersConfig = Realm.Configuration(fileURL: try! Path.inLibrary("user.realm"),
                                                        schemaVersion: 1,
                                                        deleteRealmIfMigrationNeeded: false,
                                                        objectTypes: [User.self,
                                                                      Name.self,
                                                                      Picture.self,
                                                                      Login.self])
    
    public static var users: StorageManager = {
        return StorageManager(config: usersConfig)
    }()
}

extension StorageManager {
    
    func saveObjects<T: Object>(objects: [T]) throws {
        try realm.write {
            realm.add(objects, update: .modified)
        }
    }
    
    func fetchObject<T: Object>(objectType: T.Type) -> T? {
        return realm.objects(objectType).first
    }

    func fetchObjects<T: Object>(objectType: T.Type) -> Results<T> {
        return realm.objects(objectType)
    }

    func countObjects<T: Object>(objectType: T.Type) -> Int {
        return realm.objects(objectType).count
    }

    func removeAllObjects<T: Object>(type: T.Type) {
        do {
            try realm.write {
                realm.delete(realm.objects(type))
            }
        } catch let error {
            Logger.error(error)
        }
    }
    
    func removeObjects<T: Object>(objects: [T]) {
        do {
            try realm.write {
                realm.delete(objects)
            }
        } catch let error {
            Logger.error(error)
        }
    }
}
