//
//  HTTPNetworkManager.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

enum HTTPStatusCode: Int {
    case unauthorized = 401
    case failure = 400
    case success = 200
}

enum HTTPMethod: String {
    case GET = "GET"
    case POST = "POST"
}

enum HTTPNetworkError: LocalizedError {
    case invalidURL
    case unknownError
    case unauthorized
    case failure
    case session(description: String)
    
    var errorDescription: String? {
        switch self {
        case .invalidURL: return "MyError.testError"
        case .unknownError: return "MyError.unknownError"
        case .unauthorized: return "MyError.unauthorized"
        case .session(let description): return description
        case .failure: return "MyError.failure"
        }
    }
}

fileprivate struct Constants {
    static let timeout: TimeInterval = TimeInterval(10)
}

struct HTTPNetworkManager {
    
    static func sendRequest(urlString: String, parameters: [String: String], method: HTTPMethod, completion: @escaping(Result<Data, Error>) -> Void) {
        let url = buildUrl(urlString: urlString, parameters: parameters, method: method)
        switch url {
        case .none:
            completion(.failure(HTTPNetworkError.invalidURL))
        case .some(let url):
            let request = buildRequest(url: url, parameters: parameters, method: method)
            sendRequest(request: request, completion: completion)
        }
    }
    
    private static func buildQueryItemsFrom(parameters: [String: String]) -> [URLQueryItem] {
        parameters.map { (key, value) -> URLQueryItem in
            URLQueryItem(name: key, value: value)
        }
    }
    
    private static func buildUrl(urlString: String, parameters: [String: String], method: HTTPMethod) -> URL? {
        let url = URL(string: urlString)
        switch method {
        case .GET:
            var urlComponents = URLComponents(string: urlString)
            urlComponents?.queryItems = buildQueryItemsFrom(parameters: parameters)
            return urlComponents?.url
        case .POST:
            return url
        }
    }

    private static func buildRequest(url: URL, parameters: [AnyHashable: Any], method: HTTPMethod) -> URLRequest {
        var request = URLRequest(url: url, cachePolicy: .reloadRevalidatingCacheData, timeoutInterval: Constants.timeout)
        request.httpMethod = method.rawValue
        HTTPNetworkManager.addHeaders(request: &request)
        return request
    }
    
    private static func addHeaders(request: inout URLRequest) {
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
    }
    
    private static func sendRequest(request: URLRequest, completion: @escaping(Result<Data, Error>) -> Void) {
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let response = response as? HTTPURLResponse {
                let statusCode = HTTPStatusCode(rawValue: response.statusCode)
                switch statusCode {
                case .none:
                    completion(.failure(HTTPNetworkError.unknownError))
                case .some(.failure):
                    completion(.failure(HTTPNetworkError.failure))
                case .some(.success):
                    completion(.success(data ?? Data()))
                case .some(.unauthorized):
                    completion(.failure(HTTPNetworkError.unauthorized))
                }
            }
            if let error = error {
                Logger.error(error)
                completion(.failure(HTTPNetworkError.session(description: error.localizedDescription)))
            }
        }.resume()
    }
}
