//
//  User.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class User: Object, Decodable {
    
    dynamic var id: String = UUID().uuidString
    dynamic var gender: String = ""
    dynamic var name: Name?
    dynamic var email: String = ""
    dynamic var phone: String = ""
    dynamic var cell: String = ""
    dynamic var picture: Picture?
    dynamic var login: Login?
    
    enum CodingKeys: String, CodingKey {
        case gender
        case name
        case email
        case phone
        case cell
        case picture
        case login
    }
    
    public override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(gender: String, name: Name, email: String, phone: String, cell: String, picture: Picture, login: Login) {
        self.init()
        self.gender = gender
        self.name = name
        self.email = email
        self.phone = phone
        self.cell = cell
        self.picture = picture
    }
}
