//
//  Login.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Login: Object, Decodable {
    dynamic var uuid: String = ""
    dynamic var username: String = ""
    
    enum CodingKeys: String, CodingKey {
        case uuid
        case username
    }
    
    convenience init(uuid: String, username: String) {
        self.init()
        self.uuid = uuid
        self.username = username
    }
}
