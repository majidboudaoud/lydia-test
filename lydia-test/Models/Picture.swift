//
//  Picture.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Picture: Object, Decodable {
    
    dynamic var large: String = ""
    dynamic var medium: String = ""
    dynamic var thumbnail: String = ""
    
    enum CodingKeys: String, CodingKey {
        case large
        case medium
        case thumbnail
    }

    convenience init(large: String, medium: String, thumbnail: String) {
        self.init()
        self.large = large
        self.medium = medium
        self.thumbnail = thumbnail
    }
}
