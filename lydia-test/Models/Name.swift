//
//  Name.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Name: Object, Decodable {
    dynamic var title: String = ""
    dynamic var first: String = ""
    dynamic var last: String = ""
    
    enum CodingKeys: String, CodingKey {
        case title
        case first
        case last
    }

    convenience init(title: String, first: String, last: String) {
        self.init()
        self.title = title
        self.first = first
        self.last = last
    }
}
