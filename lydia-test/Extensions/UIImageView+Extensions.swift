//
//  UIImageView+Extensions.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setImageFromUrl(urlString: String?) {
        guard let urlString = urlString else { return }
        HTTPNetworkManager.sendRequest(urlString: urlString, parameters: [:], method: .GET) { [weak self] (result) in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    let image = UIImage(data: data)
                    self?.image = image
                }
            case .failure(let error):
                Logger.error(error)
            }
        }
    }
}
