//
//  UserService.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

class UserService {
    
    func fetchRandomUsers(completion: @escaping(Result<[User], Error>) -> Void) {
        let parameters = ["results":"10"]
        HTTPNetworkManager.sendRequest(urlString: Environnement.apiString, parameters: parameters, method: .GET) { (result) in
            switch result {
            case .success(let data):
                do {
                    let response = try JSONDecoder().decode(RandomUserResponse.self, from: data)
                    completion(.success(response.results))
                } catch let error {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
