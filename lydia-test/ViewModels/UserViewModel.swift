//
//  UserViewModel.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

class UserViewModel {
    
    private let user: User
    
    init(user: User) {
        self.user = user
    }
    
    func getUserFullName() -> String? {
        guard let first = user.name?.first, let last = user.name?.last else { return nil}
        return first + " " + last
    }
    
    func getUserCellPhoneNumber() -> String? {
        return user.cell
    }
    
    func getUserPhoneNumber() -> String? {
        return user.phone
    }
    
    func getUserProfilePicture() -> String? {
        return user.picture?.large
    }
}
