//
//  UserDetailSection.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

enum UserDetailSection: Int, CaseIterable {
    case fullName
    case cell
    case phone
    
    var description: String {
        switch self {
        case .fullName:
            return "nom"
        case .cell:
            return "numéro mobile"
        case .phone:
            return "numéro fixe"
        }
    }
}
