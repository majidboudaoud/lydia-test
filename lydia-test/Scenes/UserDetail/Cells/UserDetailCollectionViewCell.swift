//
//  UserDetailCollectionViewCell.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

class UserDetailCollectionViewCell: UICollectionViewCell {
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let value: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(description: String?, value: String?) {
        self.descriptionLabel.text = description
        self.value.text = value
    }
    
    private func setupView() {
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(value)
        NSLayoutConstraint.activate([
            descriptionLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            descriptionLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 21),
            
            value.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            value.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -21),
        ])
    }
}
