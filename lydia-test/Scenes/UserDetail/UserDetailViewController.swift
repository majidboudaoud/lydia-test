//
//  UserDetailViewController.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

class UserDetailViewController: UIViewController {
    
    private lazy var collectionView = setupCollectionView()
    private let layoutDelegate = UserDetailLayoutDelegate()
    private let dataSource: UserDetailDataSource

    init(viewModel: UserViewModel) {
        self.dataSource = UserDetailDataSource(viewModel: viewModel)
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        view.addSubview(collectionView)
    }
}

extension UserDetailViewController {

    private func setupCollectionView() -> UICollectionView {
        let collectionView = UICollectionView(frame: view.frame, collectionViewLayout: LinearLayout(delegate: layoutDelegate))
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .white
        dataSource.register(collectionView: collectionView)
        return collectionView
    }
}
