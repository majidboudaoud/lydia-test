//
//  UserDetailLayoutDelegate.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

class UserDetailLayoutDelegate: LinearLayoutDelegate {
    func heightForCellAt(indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
