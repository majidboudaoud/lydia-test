//
//  UserDetailDataSource.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

class UserDetailDataSource: NSObject, UICollectionViewDataSource {
    
    var viewModel: UserViewModel
    
    init(viewModel: UserViewModel) {
        self.viewModel = viewModel
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return UserDetailSection.allCases.count
    }
    
    func register(collectionView: UICollectionView) {
        collectionView.register(cellType: UserDetailCollectionViewCell.self)
        collectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: UserDetailCollectionViewCell.self, for: indexPath)
        let section = UserDetailSection(rawValue: indexPath.section)
        switch section {
        case .fullName:
            cell.configure(description: section?.description, value: viewModel.getUserFullName())
            return cell
        case .cell:
            cell.configure(description: section?.description, value: viewModel.getUserCellPhoneNumber())
            return cell
        case .phone:
            cell.configure(description: section?.description, value: viewModel.getUserPhoneNumber())
            return cell
        case .none:
            break
        }
        return cell
    }
}
