//
//  ContactListInteractor.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

protocol ContactListDataStore {
    var userList: [User]? { get }
}

class UserListInteractor: ContactListDataStore {
    
    private let worker: UserListWorker = UserListWorker()
    private var presenter: UserListPresentationLogic?
    
    var userList: [User]?
    
    func configure(presenter: UserListPresentationLogic) {
        self.presenter = presenter
    }
    
    func fetchRandomUsers() {
        worker.fetchRandomUsers { [weak self] (result) in
            switch result {
            case .success(let userList):
                self?.userList = userList
                self?.presenter?.presentUserList(userList: userList)
            case .failure(let error):
                self?.presenter?.presentError(error: error)
            }
        }
    }
}
