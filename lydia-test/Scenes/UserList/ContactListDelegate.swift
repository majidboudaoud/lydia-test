//
//  ContactListDelegate.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

class ContactListDelegate: NSObject, UICollectionViewDelegate {
    
    private var dataStore: ContactListDataStore?
    private var router: UserListRoutingLogic?
    
    func configure(dataStore: ContactListDataStore, router: UserListRoutingLogic) {
        self.dataStore = dataStore
        self.router = router
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let user = dataStore?.userList?[indexPath.item] else { return }
        router?.navigateToUserDetail(user: user)
    }
}
