//
//  ContactListRouter.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

protocol UserListRoutingLogic {
    func navigateToUserDetail(user: User)
}

class UserListRouter: UserListRoutingLogic {
    
    private weak var viewController: UserListViewController?
    private var dataStore: ContactListDataStore?
    
    func configure(viewController: UserListViewController, dataStore: ContactListDataStore) {
        self.viewController = viewController
        self.dataStore = dataStore
    }
    
    func navigateToUserDetail(user: User) {
        let viewModel = UserViewModel(user: user)
        let detailVC = UserDetailViewController(viewModel: viewModel)
        viewController?.navigationController?.pushViewController(detailVC, animated: true)
    }
}
