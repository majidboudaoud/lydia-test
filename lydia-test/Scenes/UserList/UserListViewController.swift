//
//  ViewController.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

protocol UserListDisplayLogic: class {
    func displayContactList(viewModels: [UserViewModel])
}

class UserListViewController: UIViewController, UserListDisplayLogic {
    
    private lazy var collectionView = setupCollectionView()
    private let layoutDelegate: UserListLayoutDelegate = UserListLayoutDelegate()
    private let dataSource: UserListDataSource = UserListDataSource()
    private let interactor: UserListInteractor = UserListInteractor()
    private let delegate: ContactListDelegate = ContactListDelegate()
    private let router: UserListRouter = UserListRouter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupDependencies()
        interactor.fetchRandomUsers()
    }
    
    private func setupDependencies() {
        let presenter = UserListPresenter(viewController: self)
        interactor.configure(presenter: presenter)
        router.configure(viewController: self, dataStore: interactor)
        delegate.configure(dataStore: interactor, router: router)
    }
    
    func displayContactList(viewModels: [UserViewModel]) {
        dataSource.update(viewModels: viewModels)
        collectionView.reloadData()
    }
    
    private func setupView() {
        view.addSubview(collectionView)
    }
}

extension UserListViewController {

    private func setupCollectionView() -> UICollectionView {
        let collectionView = UICollectionView(frame: view.frame, collectionViewLayout: LinearLayout(delegate: layoutDelegate))
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .white
        dataSource.register(collectionView: collectionView)
        collectionView.delegate = delegate
        return collectionView
    }
}
