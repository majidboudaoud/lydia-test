//
//  UserCollectionViewCell.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

private struct Constants {
    static let imageWidth: CGFloat = 30
}

class UserCollectionViewCell: UICollectionViewCell {
    
    private let profilePicture: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.roundCorners(radius: Constants.imageWidth / 2)
        return imageView
    }()
    
    private let fullNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: UserViewModel) {
        self.fullNameLabel.text = viewModel.getUserFullName()
        self.profilePicture.setImageFromUrl(urlString: viewModel.getUserProfilePicture())
    }
    
    private func setupView() {
        contentView.addSubview(profilePicture)
        contentView.addSubview(fullNameLabel)
        
        NSLayoutConstraint.activate([
            profilePicture.widthAnchor.constraint(equalToConstant: Constants.imageWidth),
            profilePicture.heightAnchor.constraint(equalToConstant: Constants.imageWidth),
            profilePicture.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 15),
            profilePicture.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            fullNameLabel.leftAnchor.constraint(equalTo: profilePicture.rightAnchor, constant: 15),
            fullNameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
}
