//
//  ContactListWorker.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

struct RandomUserResponse: Decodable {
    let results: [User]
}

class UserListWorker {
    
    private let storageManager: StorageManager
    private let userService: UserService
    
    init(storageManager: StorageManager = StorageManager.users, userService: UserService = UserService()) {
        self.storageManager = storageManager
        self.userService = userService
    }
    
    func fetchRandomUsers(completion: @escaping(Result<[User], Error>) -> Void) {
            self.fetchRandomUsersFromAPI(completion: { [weak self] (result) in
                switch result {
                case .success(let users):
                    self?.deleteOldUsersObjects()
                    self?.storeUsers(users: users, completion: completion)
                case .failure(let error):
                    self?.fetchRandomUsersFromStorage { (users) in
                        if users.count > 0 {
                            completion(.success(users))
                        } else {
                            completion(.failure(error))
                        }
                    }
                }
            })
    }
    
    private func fetchRandomUsersFromAPI(completion: @escaping(Result<[User], Error>) -> Void) {
        userService.fetchRandomUsers(completion: completion)
    }
    
    private func fetchRandomUsersFromStorage(completion: @escaping([User]) -> Void) {
        DispatchQueue.main.async {
            let userList = self.storageManager.fetchObjects(objectType: User.self)
            completion(Array(userList))
        }
    }
    
    private func deleteOldUsersObjects() {
        storageManager.removeAllObjects(type: User.self)
    }
    
    private func storeUsers(users: [User], completion: @escaping (Result<[User], Error>) -> Void) {
        DispatchQueue.main.async { [weak self] in
            do {
                try self?.storageManager.saveObjects(objects: users)
                completion(.success(users))
            } catch let error {
                Logger.error(error)
                completion(.failure(error))
            }
        }
    }
    
}
