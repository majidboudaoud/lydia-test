//
//  ContactListDataSource.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

class UserListDataSource: NSObject, UICollectionViewDataSource {
    
    private var viewModels: [UserViewModel] = []
    
    func update(viewModels: [UserViewModel]) {
        self.viewModels = viewModels
    }
    
    func register(collectionView: UICollectionView) {
        collectionView.register(cellType: UserCollectionViewCell.self)
        collectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: UserCollectionViewCell.self, for: indexPath)
        cell.configure(viewModel: viewModels[indexPath.item])
        return cell
    }
}
