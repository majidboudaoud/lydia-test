//
//  ContactListPresenter.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

protocol UserListPresentationLogic: class {
    func presentUserList(userList: [User])
    func presentError(error: Error)
}

class UserListPresenter: UserListPresentationLogic {
    
    private weak var viewController: UserListDisplayLogic?
    
    init(viewController: UserListDisplayLogic) {
        self.viewController = viewController
    }
    
    func presentError(error: Error) {
        DispatchQueue.main.async {
            UIAlertController.confirm(error.localizedDescription) { (result) in
                Logger.debug("Alert closed with result: \(result)")
            }
        }
    }
    
    func presentUserList(userList: [User]) {
        let viewModels = userList.map(UserViewModel.init)
        DispatchQueue.main.async { [weak self] in
            self?.viewController?.displayContactList(viewModels: viewModels)
        }
    }
}
