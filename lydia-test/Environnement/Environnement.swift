//
//  Environnement.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

struct Environnement {
    static let apiString: String = "https://randomuser.me/api/"
}
