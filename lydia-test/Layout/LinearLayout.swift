//
//  LinearLayout.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

protocol LinearLayoutDelegate: class {
    func heightForCellAt(indexPath: IndexPath) -> CGFloat
}

class LinearLayout: UICollectionViewLayout {
    
    private weak var delegate: LinearLayoutDelegate?
    
    private var contentHeight: CGFloat = 0
    private var cellAttributes: [UICollectionViewLayoutAttributes] = []
    
    override public var collectionViewContentSize: CGSize {
        guard let collectionView = self.collectionView else { return .zero }
        return CGSize(width: collectionView.bounds.width, height: contentHeight)
    }
    
    init(delegate: LinearLayoutDelegate) {
        self.delegate = delegate
        super.init()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        guard let collectionView = self.collectionView, let delegate = delegate, self.cellAttributes.count == 0 else { return }
        for section in 0..<collectionView.numberOfSections {
            computeCellAttributesFor(section: section, collectionView: collectionView, delegate: delegate)
        }
    }
    
    private func computeCellAttributesFor(section: Int, collectionView: UICollectionView, delegate: LinearLayoutDelegate) {
        for index in 0..<collectionView.numberOfItems(inSection: section) {
            let indexPath = IndexPath(item: index, section: section)
            let cellWidth = collectionView.bounds.width
            let cellHeight = delegate.heightForCellAt(indexPath: indexPath)
            let frame: CGRect = CGRect(x: 0, y: contentHeight, width: cellWidth, height: cellHeight)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = frame
            self.contentHeight = attributes.frame.maxY
            self.cellAttributes.append(attributes)
        }
    }
    
    override public func invalidateLayout() {
        super.invalidateLayout()
        self.contentHeight = 0
        self.cellAttributes.removeAll()
    }
    
    override public func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return newBounds.width != collectionView?.bounds.width
    }
    
    override public func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cellAttributes.first { $0.indexPath == indexPath }
    }
    
    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let visibleCellAttributes = cellAttributes.filter { $0.frame.intersects(rect) }
        return visibleCellAttributes
    }
}
