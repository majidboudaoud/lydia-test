//
//  UIAlertController+Extensions.swift
//  lydia-test
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

extension UIAlertController {
  static func confirm(_ title: String, callback: @escaping (Bool) -> Void) {
    let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)

    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
      callback(false)
    })

    alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
      callback(true)
    })

    let root = UIApplication.shared.keyWindow?.rootViewController
    root?.present(alert, animated: true, completion: nil)
  }
}
