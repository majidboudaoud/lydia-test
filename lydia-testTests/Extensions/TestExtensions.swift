//
//  TestExtensions.swift
//  lydia-testTests
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import RealmSwift
@testable import lydia_test

extension StorageManager {
  func copyForTesting() -> StorageManager {
    var conf = self.configuration
    conf.inMemoryIdentifier = UUID().uuidString
    conf.readOnly = false
    return StorageManager(config: conf)
  }
}

extension StorageManager {
  func addForTesting(objects: [Object]) {
    try! self.saveObjects(objects: objects)
  }
}
