//
//  UserWorkerTests.swift
//  lydia-testTests
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import XCTest
import RealmSwift
@testable import lydia_test

class ForecastWorkerTests: XCTestCase {
    
    func testShouldSaveResultFromAPI() {
        let storageManager = StorageManager.users.copyForTesting()
        let service = UserServiceMock(completion: .success(UserMock.getUserList()))
        let worker = UserListWorker(storageManager: storageManager, userService: service)
        let expect = expectation(description: "Wait for fetchNextFiveDaysForecasts() to return")
        worker.fetchRandomUsers { (completion) in
            XCTAssertEqual(storageManager.fetchObjects(objectType: User.self).count, 1)
            expect.fulfill()
        }
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testShouldFetchFromAPI() {
        let storageManager = StorageManager.users.copyForTesting()
        let service = UserServiceMock(completion: .success(UserMock.getUserList()))
        let worker = UserListWorker(storageManager: storageManager, userService: service)
        let expect = expectation(description: "Wait for fetchNextFiveDaysForecasts() to return")
        worker.fetchRandomUsers { (completion) in
            XCTAssertEqual(service.fetchUserListWasCalled, true)
            expect.fulfill()
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShouldFetchFromDatabase() {
        let storageManager = StorageManager.users.copyForTesting()
        storageManager.addForTesting(objects: UserMock.getUserList())
        let service = UserServiceMock(completion: .failure(HTTPNetworkError.unknownError))
        let worker = UserListWorker(storageManager: storageManager, userService: service)
        let expect = expectation(description: "Wait for fetchNextFiveDaysForecasts() to return")
        worker.fetchRandomUsers { (completion) in
            switch completion {
            case .success(let users):
                XCTAssertEqual(users.count, 1)
                expect.fulfill()
            case .failure(_):
                XCTFail("should fetch one user from database")
                expect.fulfill()
            }
            
        }
        waitForExpectations(timeout: 2, handler: nil)
    }

}

