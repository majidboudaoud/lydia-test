//
//  UserTest.swift
//  lydia-testTests
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import XCTest
@testable import lydia_test

class ForecastTest: XCTestCase {

    func testJSONMapping() throws {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "UserJSON", withExtension: "json") else {
            XCTFail("Missing file: UserJSON.json")
            return
        }
        let json = try Data(contentsOf: url)
        let forecasts = try JSONDecoder().decode(RandomUserResponse.self, from: json)
        XCTAssertEqual(forecasts.results.count, 10)
    }
}
