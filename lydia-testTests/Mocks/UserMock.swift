//
//  UserListMock.swift
//  lydia-testTests
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import RealmSwift
@testable import lydia_test

struct UserMock {
    
    static func getUserList() -> [User] {
        return [User(gender: "male",
                     name: Name(title:"Mr", first: "Majid", last: "Boudaoud"),
                     email: "majid.boudaoud@gmail.com",
                     phone: "0787450053",
                     cell: "0148214279",
                     picture: Picture(),
                     login: Login())]
    }
}
