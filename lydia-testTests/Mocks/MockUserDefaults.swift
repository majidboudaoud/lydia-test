//
//  MockUserDefaults.swift
//  lydia-testTests
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

class MockUserDefaults : UserDefaults {
    
    convenience init() {
      self.init(suiteName: "Mock User Defaults")!
    }
    
    override init?(suiteName suitename: String?) {
      UserDefaults().removePersistentDomain(forName: suitename!)
      super.init(suiteName: suitename)
    }
}
