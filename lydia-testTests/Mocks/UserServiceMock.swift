//
//  UserServiceMock.swift
//  lydia-testTests
//
//  Created by Majid Boudaoud on 30/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
@testable import lydia_test

class UserServiceMock: UserService {
    
    var fetchUserListWasCalled: Bool = false
    let completion: Result<[User], Error>
    
    init(completion: Result<[User], Error>) {
        self.completion = completion
    }
    
    override func fetchRandomUsers(completion: @escaping (Result<[User], Error>) -> Void) {
        fetchUserListWasCalled = true
        completion(self.completion)
    }
}
